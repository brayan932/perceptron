#REDES NEURONALES
Para crear un algoritmo de aprendizaje (en este caso una compuerta lógica OR)
se usara un tipo de neurona artificial llamada perceptron con 2 entras.

#REPRESENTACIONES
--  x1 = entrada 1. 
--  x2 = entrada 2.
--  w1 = peso 1.
--  w2 = pesos 2.
--  o = umbral.
--  wx = (x1 * w1) + (x2 * w2) + (-1 * 0).

#FUNCIÓN
La red neuronal aprenderá por medio de pesos variables hasta encontrar 
aleatoria mente números que concuerden con la salida de la compuerta lógica  

#OR
 //1  1 =   1
 //1 -1 =   1
 //-1  1 =   1
 //-1 -1 =  -1